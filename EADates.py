# coding: utf-8

""" 

A module for conversions from UTC to NZ standard time (NZST), NZ local time (NZDT), and TP. Built on pytz. 
Written by Osmond Bishop


Usage
-----
Use get_dates(x) with a UTC datetime.datetime arg to derive NZ standard time, local time, TP and oter values

Returns a DataFrame

Datetime strings are generally in '%d-%b-%Y %H:%M' format throughout this notebook.

; is used to differentiate the duplicated hour in NZ local time during daylight savings. 

E.g. On 5 April 2015, 2:00am NZ local time = 1:00am NZ standard time, and 2;00am NZ local time = 2:00am NZ standard time

See the usage examples at the bottom of this notebook.


Requirements
------------
pandas, numpy and pytz


To be added
-----------
5 min periods


"""

from datetime import datetime, time, timedelta
import pytz
import numpy as np
import pandas as pd

def get_offset(dt):
    """ Returns a timedelta of the UTC offset from a local datetime """
    return pytz.timezone('Pacific/Auckland').localize(dt).utcoffset()

def convert_utc_dt_to_nzdt_dt(date_utc_dt): #convert_utc_to_nzt
    """ Convert a UTC datetime to NZ local (civil) datetime """
    return date_utc_dt + get_offset(date_utc_dt+timedelta(hours=12))

def convert_utc_dt_to_nzst_dt(date_utc_dt): #convert_utc_to_nzt_std
    """ Convert a UTC datetime to NZ standard datetime """
    return date_utc_dt + timedelta(hours=12)

def convert_utc_dt_to_nzdt_str(date_utc_dt, gdxfmt='%d-%b-%Y %H:%M'): #convert_utc_dt_to_local_nzt_str
    """ Convert a UTC datetime to NZ local datetime-string (repeated hour during DS indicated using ; to replace : )"""
    if convert_utc_dt_to_nzdt_dt(date_utc_dt) == convert_utc_dt_to_nzdt_dt(date_utc_dt+timedelta(hours=-1)):
        gdxfmt = gdxfmt.replace(':',';')     
    date_nzt_dt = convert_utc_dt_to_nzdt_dt(date_utc_dt)    
    return datetime.strftime(date_nzt_dt,gdxfmt)

def convert_utc_dt_to_nzst_str(date_utc_dt, gdxfmt='%d-%b-%Y %H:%M'): #convert_utc_dt_to_std_nzt_str
    """ Convert a UTC datetime to NZ standard datetime-string"""
    date_nzt_dt = convert_utc_dt_to_nzst_dt(date_utc_dt)    
    return datetime.strftime(date_nzt_dt,gdxfmt)

def convert_dt_to_str(dt, gdxfmt='%d-%b-%Y %H:%M'):
    return datetime.strftime(dt,gdxfmt)

def convert_str_to_dt(date_str, gdxfmt='%d-%b-%Y %H:%M'):
    """ Convert a datetime string to datetime.datetime. Assumes a colon is used as hour-minute separator """
    if ';' in date_str:
        gdxfmt = gdxfmt.replace(':',';')
    return datetime.strptime(date_str, gdxfmt)

def num_tp(dt):
    """ Takes a datetime (NZ Civil time) and returns number of TP"""
    #print dt.hour
    #print dt.minute
    date1=datetime(dt.year,dt.month,dt.day,0,0)
    date2=datetime(dt.year,dt.month,dt.day,23,59)
    tzdelta = get_offset(date1) - get_offset(date2)
    if tzdelta > timedelta(hours=0):
        return 50
    elif tzdelta < timedelta(hours=0):
        return 46
    else:
        return 48
    
def get_tp(ufc_dt):
    local_dt = convert_str_to_dt(convert_utc_dt_to_nzdt_str(ufc_dt))
    offset = local_dt-ufc_dt
    number_of_tp = num_tp(local_dt)

    h=local_dt.hour
    m=local_dt.minute
    tp = 2*h + np.ceil(m/30)+1
    
    if tp>48:
        tp=tp-48
    if number_of_tp==50 and offset.seconds==12*60*60: # gain two trading periods
        tp=tp+2
    elif number_of_tp==46 and offset.seconds==13*60*60: #lose two trading periods
        tp=tp-2

    return int(tp)

def convert_nzdt_str_to_utc_dt(date_nzdt_str):
    dt = convert_str_to_dt(date_nzdt_str)
    offset = get_offset(dt)
    change_in_offset = offset - get_offset(dt-timedelta(hours=1)) 
    
    if change_in_offset < timedelta(hours=0) and ':' in date_nzdt_str:
        return dt - get_offset(dt) - timedelta(hours=1)
    else:
        return dt - get_offset(dt)

def get_dates(date_utc_dt, period_minutes=30, num_periods=1):
    """ Derives standard and local NZ datetimes and trading period from a UTC datetime. 
    
    Parameters
    ----------
    date_utc_dt: datetime.datetime
        A UTC datetime. The first row starts from this datetime
    
    period_minutes: int
        Determines the number of minutes per period
        
    num_periods: int
        Number of periods to return (the length of the dataframe)
    
    Returns
    -------
    pd.DataFrame
    columns: UTC,UTC_DT,NZST,NZST_DT,NZDT,NZDT_DT,TP,NUM_TP,UTC_OFFSET
    All date string formats are '%d-%b-%Y %H:%M'
    Duplicate dates during daylight savings replace ':' with ';'
    
    UTC: Co-ordinated Universal Time string 
    UTC_DT: Co-ordinated Universal Time datetime.datetime
    NZST: NZ Standard time string
    NZST_DT: NZ Standard time datetime.datetime.
    NZDT: NZ Local time string
    NZDT_DT: NZ Local time datetime.datetime
    TP: 30 minute trading period
    NUM_TP: number of trading period for given date
    UTC_OFFSET: number of hours that NZ local time is offset on UTC
    
    """
    
    df = pd.DataFrame(columns=[['UTC']]) # Base dataframe
    for d in [date_utc_dt + timedelta(hours=x*period_minutes/60.0) for x in np.arange(0,num_periods)]:   
        df = df.append({'UTC_DT': d}, ignore_index=True)
    
    df['UTC']=df['UTC_DT'].apply(lambda x: datetime.strftime(x,'%d-%b-%Y %H:%M'))
    df['NZST']=df['UTC_DT'].apply(lambda x: convert_utc_dt_to_nzst_str(x))
    df['NUM_TP'] = df['UTC_DT'].apply(lambda x: num_tp(convert_utc_dt_to_nzdt_dt(x)))    
    df['NZST_DT'] = df['NZST'].apply(lambda x: convert_str_to_dt(x))    
    df['NZDT']=df['UTC_DT'].apply(lambda x:convert_utc_dt_to_nzdt_str(x))
    df['NZDT_DT'] = df['UTC_DT'].apply(lambda x: convert_str_to_dt(convert_utc_dt_to_nzdt_str(x)))    
    df['UTC_OFFSET'] = df['NZDT_DT'] - df['UTC_DT']
    df['TP'] = df['UTC_DT'].apply(lambda x: get_tp(x))    
    
    return df[['UTC','UTC_DT','NZST','NZST_DT','NZDT','NZDT_DT','TP','NUM_TP','UTC_OFFSET']]

def get_date_and_tp(date_from, date_to):
    """ 
    Wrapper function for get_dates
    Takes a start NZDT and end NZDT (local times) and returns df including TP 
        
    """
    
    num_periods = (convert_nzdt_str_to_utc_dt(date_to) - convert_nzdt_str_to_utc_dt(date_from)).seconds/(1800)+1    
    df=get_dates(convert_nzdt_str_to_utc_dt(date_from), period_minutes=30, num_periods=num_periods)[['NZDT','TP']]
    df['NZDT']=df['NZDT'].apply(str.upper)
    df['TP'] = df['TP'].apply(lambda x: 'TP'+str(x))
    return df.rename(columns={'NZDT':'INTERVAL_30'})

# See notebook on git for examples
# Original purpose of this was to achieve the following query output without internal connection to our DW

"""
SELECT 
 [DIMDT_PERIOD_GDX]
,[DIM_INTERVAL] 
,[NZST]
FROM [DWCommon].[com].[DIM_Date_Time] a
WHERE a.DIMDT_DATE between '20150405' and '20150405'
and a.DIM_INTERVAL between '05-Apr-2015 01:00' and '05-Apr-2015 04:00'

"""

# Output
"""
 DIMDT_PERIOD_GDX       DIM_INTERVAL
0                  TP3  05-Apr-2015 01:00
1                  TP4  05-Apr-2015 01:30
2                  TP5  05-Apr-2015 02:00
3                  TP6  05-Apr-2015 02:30
4                  TP7  05-Apr-2015 02;00
5                  TP8  05-Apr-2015 02;30
6                  TP9  05-Apr-2015 03:00
7                 TP10  05-Apr-2015 03:30
8                 TP11  05-Apr-2015 04:00
"""

